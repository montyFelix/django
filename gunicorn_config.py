import os

timeout = int(os.getenv("GUNICORN_TIMEOUT", 120))
