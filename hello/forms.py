from django import forms
from functools import partial

DateInput = partial(forms.DateTimeInput, {'class': 'datepicker'})

class ContactForm(forms.Form):
	contact_name = forms.CharField(required=True)
	contact_email = forms.EmailField(required=True)
	content = forms.CharField(
		required=True,
		widget=forms.Textarea
	)

class SmsForm(forms.Form):
	event_type = forms.CharField(label="Мероприятие", widget=forms.TextInput(attrs={'placeholder': 'Корпоратив? Свадьба? Свидание?:)'}))
	contact_name = forms.CharField(label="Имя", required=True, widget=forms.TextInput(attrs={'placeholder': 'Пан(і)'}))
	contact_telephone = forms.CharField(label="Контактный телефон", required=True, widget=forms.TextInput(attrs={'placeholder': '0631234567', 'pattern': '^(0)(50|63|66|67|68|73|91|92|93|94|95|96|97|98|99)[0-9]{7}|^(\+380)(50|63|66|67|68|73|91|92|93|94|95|96|97|98|99)[0-9]{7}$', 'title': 'Введіть будь ласка номер у форматі 0ххххххххх (10 символів) або+380ххххххххх (13 символів)'}))
	event_date = forms.DateTimeField(label="Дата мероприятия", input_formats=['%m/%d/%Y %H:%M'], widget=DateInput())
