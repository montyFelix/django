from django.shortcuts import render
from django.http import HttpResponse

from .models import Greeting

import requests


from django.conf import settings
from django.contrib import messages
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMessage
import random
import json
#from .forms import ContactForm, SmsForm
from .forms import SmsForm
from django.core.mail import send_mail
from feeds.forms import FeedForm
from feeds.models import Feed
from feeds.views import feed, feed_delete
from twilio.rest import Client
from django.http import HttpResponse


# Create your views here.
def index(request):
    # return HttpResponse('Hello from Python!')
    return render(request, 'index.html')

def home(request):
    title = "Кофейня Espresso На Филатова"
    ramdo = random.randint(1, 9)
    ramdo = random.randint(0, len(Feed.objects.all())-1)
    full = Feed.objects.all()[ramdo]
    form = SmsForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            ACCOUNT_SID = getattr(settings, 'ACCOUNT_SID', None)
            AUTH_TOKEN = getattr(settings, 'AUTH_TOKEN', None)
            client = Client(ACCOUNT_SID, AUTH_TOKEN)
            text = "Мероприятие: {}, Заказчик: {}, Телефон: {}, Дата: {},".format(form.cleaned_data['event_type'],
                                        form.cleaned_data['contact_name'],
                                        form.cleaned_data['contact_telephone'],
                                        form.cleaned_data['event_date'])
            client.messages.create(
                body=text,
                to="+380xxxxxxxxx",
                from_="+19108176931")
            messages.success(request, "Successfuly Sended")
    context = {
	    "template_title": title,
        'full': full,
        "navbar": "home",
        'form': form,
    }
    return render(request, "home.html", context)

def about(request):
    title = "Кофейня Espresso На Филатова"
    context = {
        "template_title": title,
        "navbar": "about",
    }
    return render(request, "about.html", context)

def service(request):
    title = "Сервис Кафе Espresso"
    context = {
    "template_title": title,
    "navbar": "service",
    }
    return render(request, "service.html", context)



def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})
