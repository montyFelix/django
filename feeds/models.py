from django.db import models

# Create your models here.
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
# Create your models here.

class Feed(models.Model):
    id = models.AutoField(primary_key=True)
    author = models.CharField(max_length=120)
    feed = models.TextField(max_length=520)
    timestamp = models.DateTimeField(default=timezone.now())
    def __unicode__(self):
        return {self.author: self.feed}

    def __str__(self):
        return self.feed

    def __repr__(self):
        return self.feed
