from django.contrib import admin

# Register your models here.
from .models import Feed
# Register your models here.
class FeedModelAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", 'author', 'feed', "timestamp"]
    list_filter = ['timestamp']
    search_fields = ['author', 'feed']
    class Meta:
        model = Feed
admin.site.register(Feed, FeedModelAdmin)
