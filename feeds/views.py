from django.shortcuts import render

# Create your views here.
from django.conf import settings
# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .forms import FeedForm
from .models import Feed
import random

def feed(request):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    form = FeedForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        messages.success(request, "Successfuly Created")
    foll = []
    for i in range(0, len(Feed.objects.all())):
        foll.append((Feed.objects.all()[i].id, Feed.objects.all()[i]))
    full = Feed.objects.all()
    context = {
        'form': form,
        'full': full,
        'foll': foll,
    }
    return render(request, "feed_form.html", context)

def feed_delete(request, id=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Feed, id=id)
    instance.delete()
    messages.success(request, "Successfuly Deleted")
    return redirect('feed')
