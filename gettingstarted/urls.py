from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from django.views.static import serve
admin.autodiscover()



import hello.views
import feeds.views
from feeds.views import feed, feed_delete

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^$', hello.views.home, name='home'),
    url(r'^service/$', hello.views.service, name='service'),
    url(r'^about/$', hello.views.about, name='about'),
    url(r'^contact/', include('contact_form.urls')),
    url(r'^db', hello.views.db, name='db'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^feed/', feeds.views.feed, name='feed'),
    url(r'^delete/(?P<id>\d+)/$', feeds.views.feed_delete, name='feed_delete'),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
    url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
]
